from django.contrib.auth.models import User, Permission
from django.test import TestCase

from smi_unb.users.models import UserPermissions


class TestUsersModels(TestCase):
    def setUp(self):
        self.superuser = User(
            username='superuser',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

        self.normaluser = User(
            username='normaluser',
            email="test@test.com",
            is_superuser=False,
        )
        self.normaluser.set_password('12345')
        self.normaluser.save()

    def test_given_perm_manager_buildings(self):
        manager_buildings = Permission.objects.get(
            codename='manager_buildings')
        self.normaluser.user_permissions.add(manager_buildings)

        self.assertTrue(self.normaluser.has_perm('users.manager_buildings'))

    def test_given_perm_manager_transductors(self):
        manager_transductors = Permission.objects.get(
            codename='manager_transductors')
        self.normaluser.user_permissions.add(manager_transductors)

        self.assertTrue(
            self.normaluser.has_perm('users.manager_transductors'))

    def test_add_all_user_permissions(self):
        user = self.create_user(
            'test', 'password', False, 'teste@email.com', [])

        self.assertFalse(user.has_perm('users.manager_buildings'))
        self.assertFalse(user.has_perm('users.manager_transductors'))

        UserPermissions.add_all_user_permissions(user)

        user_with_permissios = User.objects.get(username='test')

        self.assertTrue(
            user_with_permissios.has_perm('users.manager_buildings'))
        self.assertTrue(
            user_with_permissios.has_perm('users.manager_transductors'))

    def test_remove_all_user_permissions(self):
        all_permissions = UserPermissions.codenames
        user = self.create_user(
            'test', 'password', False, 'teste@email.com', all_permissions)

        self.assertTrue(user.has_perm('users.manager_buildings'))
        self.assertTrue(user.has_perm('users.manager_transductors'))

        UserPermissions.remove_all_user_permissions(user)

        user_without_permissions = User.objects.get(username='test')

        self.assertFalse(
            user_without_permissions.has_perm('users.manager_buildings'))
        self.assertFalse(
            user_without_permissions.has_perm('users.manager_transductors'))

    def test_remove_specific_user_permissions(self):
        permissions = ['manager_buildings', 'manager_transductors']
        user = self.create_user(
            'test', 'password', False, 'teste@email.com', permissions)

        self.assertTrue(user.has_perm('users.manager_buildings'))
        self.assertTrue(user.has_perm('users.manager_transductors'))

        UserPermissions.remove_user_permissions(user, permissions)

        user_without_permissions = User.objects.get(username='test')

        self.assertFalse(
            user_without_permissions.has_perm('users.manager_buildings'))
        self.assertFalse(
            user_without_permissions.has_perm('users.manager_transductors'))

    def test_add_specific_user_permissions(self):
        user = self.create_user(
            'test', 'password', False, 'teste@email.com', [])

        self.assertFalse(user.has_perm('users.manager_buildings'))
        self.assertFalse(user.has_perm('users.manager_transductors'))

        permissions = ['manager_buildings', 'manager_transductors']
        UserPermissions.add_user_permissions(user, permissions)

        user_with_permissions = User.objects.get(username='test')

        self.assertTrue(
            user_with_permissions.has_perm('users.manager_buildings'))
        self.assertTrue(
            user_with_permissions.has_perm('users.manager_transductors'))

    def create_user(
        self, username, password, is_superuser, email, permissions_codenames
    ):
        new_user = User()
        new_user.username = username
        new_user.set_password = password
        new_user.is_superuser = is_superuser
        new_user.email = email
        new_user.save()

        for codename in permissions_codenames:
            permission = Permission.objects.get(codename=codename)
            new_user.user_permissions.add(permission)

        return new_user
